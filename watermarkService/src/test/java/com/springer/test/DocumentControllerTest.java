package com.springer.test;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.webAppContextSetup;

import java.io.IOException;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.mock.http.MockHttpOutputMessage;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.web.context.WebApplicationContext;

import com.springer.Application;
import com.springer.DocumentRepository;
import com.springer.model.Book;
import com.springer.model.Document;
import com.springer.model.Journal;
import com.springer.model.Topic;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = Application.class)
@WebAppConfiguration
public class DocumentControllerTest {

	private MediaType contentType = new MediaType(MediaType.APPLICATION_JSON.getType(),
			MediaType.APPLICATION_JSON.getSubtype(), Charset.forName("utf8"));

	private MockMvc mockMvc;

	private HttpMessageConverter mappingJackson2HttpMessageConverter;

	private Document document;

	private List<Document> documentList = new ArrayList<Document>();

	@Autowired
	private DocumentRepository documentRepository;

	@Autowired
	private WebApplicationContext webApplicationContext;

	@Autowired
	void setConverters(HttpMessageConverter<?>[] converters) {

		this.mappingJackson2HttpMessageConverter = Arrays.asList(converters).stream()
				.filter(hmc -> hmc instanceof MappingJackson2HttpMessageConverter).findAny().orElse(null);

		assertNotNull("the JSON message converter must not be null", this.mappingJackson2HttpMessageConverter);
	}

	/**
	 * Adds some data to test.
	 * 
	 * @throws Exception
	 */
	@Before
	public void setup() throws Exception {
		this.mockMvc = webAppContextSetup(webApplicationContext).build();

		this.documentRepository.deleteAllInBatch();

		this.documentList.add(documentRepository.save(new Book("The Dark Code", "Bruce Wayne", Topic.SCIENCE)));
		this.documentList.add(documentRepository.save(new Book("How to make money", "Dr. Evil", Topic.BUSINESS)));
		this.documentList.add(documentRepository.save(new Journal("Journal of human flight routes", "Clark Kent")));
	}

	/**
	 * Tests that the user can access all the documents with the expected
	 * values, i.e. the watermark is empty first time the user access the document.
	 * 
	 * This functionality is done for testing purposes.
	 * 
	 * Steps:
	 * <ol>
	 * <li>The client makes a call to the root URL</li>
	 * <li>The system shows all the documents available</li>
	 * </ol>
	 * <br>
	 * Expected: There are three documents present and none of them contains a
	 * watermark.
	 * 
	 * @throws Exception
	 */
	@Test
	public void readAllDocuments() throws Exception {
		mockMvc.perform(get("/watermark")).andExpect(status().isOk()).andExpect(content().contentType(contentType))
				.andExpect(jsonPath("$", hasSize(3)))
				.andExpect(jsonPath("$[0].ticket", is(this.documentList.get(0).getTicket().intValue())))
				.andExpect(jsonPath("$[0].title", is(this.documentList.get(0).getTitle())))
				.andExpect(jsonPath("$[0].watermark", is(equalTo(null))))
				.andExpect(jsonPath("$[0].author", is(this.documentList.get(0).getAuthor())))

				.andExpect(jsonPath("$[1].ticket", is(this.documentList.get(1).getTicket().intValue())))
				.andExpect(jsonPath("$[1].title", is(this.documentList.get(1).getTitle())))
				.andExpect(jsonPath("$[1].watermark", is(equalTo(null))))
				.andExpect(jsonPath("$[1].author", is(this.documentList.get(1).getAuthor())))

				.andExpect(jsonPath("$[2].ticket", is(this.documentList.get(2).getTicket().intValue())))
				.andExpect(jsonPath("$[2].title", is(this.documentList.get(2).getTitle())))
				.andExpect(jsonPath("$[2].watermark", is(equalTo(null))))
				.andExpect(jsonPath("$[2].author", is(this.documentList.get(2).getAuthor())));
	}

	/**
	 * Tests the access using a ticket to a content document.<br>
	 * Steps:
	 * <ol>
	 * <li>The client uses a ticket to retrieve a document that does not have a
	 * watermark.</li>
	 * <li>The client has to wait until the watermark is created and assigned to
	 * that document.</li>
	 * <li>The document is retrieved with the watermark.</li>
	 * <li>The client uses the ticket to retrieve the same document (it has the
	 * watermark).</li>
	 * <li>The client does not have to wait for the watermark to be
	 * generated.</li>
	 * <li>The document is retrieved with the watermark.</li>
	 * </ol>
	 * <br>
	 * Expected: the execution time should be smaller when the user access a
	 * document that already contains the watermark.
	 * 
	 * @throws Exception
	 */
	@Test
	public void readDocument() throws Exception {
		// Start the clock
		long start = System.currentTimeMillis();

		mockMvc.perform(get("/watermark" + "/" + this.documentList.get(0).getTicket()))//
				.andExpect(status().isOk()) //
				.andExpect(jsonPath("$.ticket", is(this.documentList.get(0).getTicket().intValue()))); //
		long timeCreatingWatermark = (System.currentTimeMillis() - start);

		// Restart the clock
		start = System.currentTimeMillis();
		mockMvc.perform(get("/watermark" + "/" + this.documentList.get(0).getTicket())) //
				.andExpect(status().isOk())//
				.andExpect(jsonPath("$.ticket", is(this.documentList.get(0).getTicket().intValue()))); //

		long timeWithoutCreatingWatermark = (System.currentTimeMillis() - start);
		assertTrue(timeWithoutCreatingWatermark < timeCreatingWatermark);
	}

	/**
	 * Tests that a watermark is created for a document.<br>
	 * Steps:
	 * <ol>
	 * <li>The client makes a call to the root URL</li>
	 * <li>The document we are testing does not have a watermark</li>
	 * <li>The client uses a ticket to retrieve the document under test.</li>
	 * <li>The client makes a call to the root URL</li>
	 * <li>The document is retrieved with the watermark.</li>
	 * </ol>
	 * <br>
	 * Expected: Client should retrieve a document with the created watermark.
	 * 
	 * @throws Exception
	 */
	@Test
	public void watermarkCreation() throws Exception {
		// check that a document does not have the watermark
		mockMvc.perform(get("/watermark")).andExpect(status().isOk())//
				.andExpect(content().contentType(contentType)) //
				.andExpect(jsonPath("$[1].ticket", is(this.documentList.get(1).getTicket().intValue()))) //
				.andExpect(jsonPath("$[1].watermark", is(equalTo(null))));

		// obtain the watermark
		mockMvc.perform(get("/watermark" + "/" + this.documentList.get(1).getTicket()))//
				.andExpect(status().isOk()); // ;

		// check that the document have the watermark after the first
		// access
		mockMvc.perform(get("/watermark")).andExpect(status().isOk()) //
				.andExpect(content().contentType(contentType))//
				.andExpect(jsonPath("$[1].ticket", is(this.documentList.get(1).getTicket().intValue()))) //
				.andExpect(jsonPath("$[1].watermark", is(equalTo(
						"{content:\"book\", title:\"How to make money\", author:\"Dr. Evil\", topic:\"business\"}"))));
	}
}
