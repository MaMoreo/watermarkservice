package com.springer;

import java.util.concurrent.Future;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.AsyncResult;
import org.springframework.stereotype.Service;

import com.springer.model.Document;

/**
 * The Watermark Asynchronous Service.
 * 
 * @author Miguel Moreo
 *
 */
@Service
public class WatermarkService {

	private static final Logger logger = LoggerFactory.getLogger(WatermarkService.class);

	/**
	 * Creates and sets the appropriate Watermark for a Document.
	 * 
	 * @param document
	 *            The document to generate the Watermark.
	 * @return Returns a {@link Future} objetc that will contain the Document
	 *         with the Watermark when the creation operation finishes.
	 * 
	 * @throws InterruptedException
	 */
	@Async
	public Future<Document> createWatermark(Document document) throws InterruptedException {
		logger.info("Looking up " + document.getTicket());
		// Artificial delay of 1s for demonstration purposes
		Thread.sleep(1000L);
		logger.info("Finished the Watermark for " + document.getTicket());
		document.setWatermark(document.createWatermark());
		return new AsyncResult<>(document);
	}
}
