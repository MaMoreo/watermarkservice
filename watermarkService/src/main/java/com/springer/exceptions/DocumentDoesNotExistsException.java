package com.springer.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.NOT_FOUND)
@SuppressWarnings("serial")
public class DocumentDoesNotExistsException extends RuntimeException {

	/**
	 * The document wit this ticket does not exist.
	 */
	public DocumentDoesNotExistsException(Long ticket) {
		super("The document with ticket '" + ticket + "' was not found in the system.");
	}
}
