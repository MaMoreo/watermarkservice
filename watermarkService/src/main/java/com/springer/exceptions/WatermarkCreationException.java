package com.springer.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.NOT_FOUND)
@SuppressWarnings("serial")
public class WatermarkCreationException extends RuntimeException {

	/**
	 * There was a problem generating this watermark.
	 */
	public WatermarkCreationException(Long ticket) {
		super("Problem creating watermark for'" + ticket + "'.");
	}
}
