package com.springer.model;

import javax.persistence.DiscriminatorColumn;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.Table;
import javax.persistence.TableGenerator;

/**
 * Entity in the DATABASE that represents a document. <br>
 * <br>
 * The Strategy used is Single Table, this means, all the {@link Book}s and
 * {@link Journal}s (Child Classes that inherit from Document) will be stored in
 * a single database table.<br>
 * <br>
 * 
 * We will use a Discriminator Value with the proper value to each one.<br>
 * 
 * @author Miguel Moreo
 */
@Entity
@Table(name = "DOCUMENT")
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name = "DOCUMENT_TYPE")
public abstract class Document {

	@TableGenerator(name = "DOCUMENT_GEN", table = "ID_GEN", pkColumnName = "GEN_NAME", valueColumnName = "GEN_VAL", allocationSize = 1)
	@Id
	@GeneratedValue(strategy = GenerationType.TABLE, generator = "DOCUMENT_GEN")
	private Long ticket;   // will represent the Tickeck in our logic
 
	protected String title;
	protected String author;
	protected String watermark;

	/**
	 * Default Constructor for JPA
	 */
	Document() {
		// JPA Only
	}

	// Getters and Setters
	
	public Long getTicket() {
		return ticket;
	}

	public void setTicket(Long ticket) {
		this.ticket = ticket;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getAuthor() {
		return author;
	}

	public void setAuthor(String author) {
		this.author = author;
	}

	public String getWatermark() {
		return watermark;
	}

	public void setWatermark(String watermark) {
		this.watermark = watermark;
	}

	/**
	 * Creates the proper watermark for this document.
	 * 
	 * @return a watermark for this document.
	 */
	public abstract String createWatermark();
}
