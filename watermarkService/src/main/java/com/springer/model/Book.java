package com.springer.model;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

/**
 * The Book representation. It contains title, author, topic and watermark. It
 * represents an entity in the Database.
 * 
 * @author Miguel Moreo
 */
@Entity
@DiscriminatorValue(value = "Book")
public class Book extends Document {

	private Topic topic;

	/**
	 * Constructor for JPA
	 */
	public Book() {
		// JPA only
	}

	/**
	 * Constructor using fields from parent.
	 * 
	 * @param title
	 *            The title of this document.
	 * @param author
	 *            The author of this document.
	 * @param topic
	 *            The {@link Topic} of this document
	 */
	public Book(String title, String author, Topic topic) {
		this.title = title;
		this.author = author;
		this.topic = topic;
	}

	// Getters and Setters

	public Topic getTopicValue() {
		return topic;
	}

	public void setTopicValue(Topic topic) {
		this.topic = topic;
	}

	@Override
	public String createWatermark() {
		return "{content:\"book\", title:\"" + title + "\", author:\"" + author + //
				"\", topic:\"" + getTopicValue().toString().toLowerCase() + "\"}";
	}
}
