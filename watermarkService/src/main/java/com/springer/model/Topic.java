package com.springer.model;

/**
 * Represents the topics a Book can have.
 * 
 * The available topics are
 * <ol>
 * business, science and media.
 * </ol>
 * 
 * @author Miguel Moreo
 *
 */
public enum Topic {
	BUSINESS, SCIENCE, MEDIA
}
