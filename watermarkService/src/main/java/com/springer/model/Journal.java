package com.springer.model;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

/**
 * The Journal representation. It contains title, author and watermark. It
 * represents an entity in the Database.
 * 
 * @author Miguel Moreo
 */
@Entity
@DiscriminatorValue(value = "Journal")
public class Journal extends Document {

	public Journal() {
		// For JPA
	}

	/**
	 * Constructor using fields from parent.
	 * 
	 * @param title
	 *            The title of this document.
	 * @param author
	 *            The author of this document.
	 */
	public Journal(String title, String author) {
		this.title = title;
		this.author = author;
	}

	@Override
	public String createWatermark() {
		return "{content:\"journal\", title:\"" + title + "\", author\":" + author + "\"}";
	}

}
