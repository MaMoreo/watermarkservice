package com.springer;

import java.util.concurrent.Executor;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.scheduling.annotation.AsyncConfigurerSupport;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;

import com.springer.model.Book;
import com.springer.model.Journal;
import com.springer.model.Topic;

/**
 * Spring Boot Application Class. Contains initialization data.
 * 
 * @author Miguel Moreo
 */
@SpringBootApplication
@EnableAsync
public class Application extends AsyncConfigurerSupport {

	public static void main(String[] args) {
		SpringApplication.run(Application.class, args);
	}

	@Override
	public Executor getAsyncExecutor() {
		ThreadPoolTaskExecutor executor = new ThreadPoolTaskExecutor();
		executor.setCorePoolSize(2);
		executor.setMaxPoolSize(2);
		executor.setQueueCapacity(500);
		executor.setThreadNamePrefix("WaterService-");
		executor.initialize();
		return executor;
	}

	/**
	 * Once started, Spring Boot will call all beans of type CommandLineRunner,
	 * giving them a callback.
	 * 
	 * This method populates the H2 embedded database with Test data.
	 * 
	 * @param documentRepository
	 *            Spring will inject this automatically
	 */
	@Bean
	CommandLineRunner run(DocumentRepository documentRepository) {
		CommandLineRunner c = new CommandLineRunner() {
			public void run(String... args) {
				documentRepository.save(new Book("The Dark Code", "Bruce Wayne", Topic.SCIENCE));
				documentRepository.save(new Book("How to make money", "Dr. Evil", Topic.BUSINESS));
				documentRepository.save(new Journal("Journal of human flight routes", "Clark Kent"));
			}
		};
		return c;
	}
}
