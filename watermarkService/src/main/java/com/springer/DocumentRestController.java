package com.springer;

import java.util.Collection;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.springer.exceptions.DocumentDoesNotExistsException;
import com.springer.exceptions.WatermarkCreationException;
import com.springer.model.Document;

/**
 * Document REST Controller.
 * 
 * 
 * Answers to the call http://localhost:8080/watermark/ showing all the
 * documents (Testing purposes) <br>
 * <br>
 * Answers to the call http://localhost:8080/watermark/{ticket} calling an
 * asynchronous method to generate the watermark if it is not present.
 * 
 * @author Miguel Moreo
 */
@RestController
@RequestMapping("/watermark")
public class DocumentRestController {

	private final DocumentRepository documentRepository;
	private final WatermarkService watermarkService;
	private static final Logger logger = LoggerFactory.getLogger(DocumentRestController.class);

	@Autowired
	public DocumentRestController(DocumentRepository documentRepository, WatermarkService watermarkService) {
		this.watermarkService = watermarkService;
		this.documentRepository = documentRepository;
	}

	/**
	 * Gets all the documents in the system.<br>
	 * <br>
	 * Replies to http://localhost:8080/watermark/ (GET)
	 * 
	 * @return A collection with all the documents
	 */
	@RequestMapping(method = RequestMethod.GET)
	Collection<Document> getAllDocuments() {
		return documentRepository.findAll();
	}

	@RequestMapping(method = RequestMethod.GET, value = "/{ticket}")
	public Document findDocument(@PathVariable Long ticket) throws InterruptedException {
		/**
		 * This method is not Thread Safe. For this example we suppose the Front
		 * End will take care of this.
		 * 
		 * We are making some operations that can be dangerous in a multi-thread
		 * environment, for example we assign the watermark to the document, set
		 * it to the document and save it in the database.
		 * 
		 * We could do a synchronization block to prevent this operations to
		 * happen several times if different threads try to retrieve the same
		 * document at the same time.
		 */
		Document document = documentRepository.findOne(ticket);
		if (document == null){
			throw new DocumentDoesNotExistsException(ticket);
		}
		
		if (document.getWatermark() != null) {
			return document;
		}

		// ASYCN
		Future<Document> documentWithWatermark = watermarkService.createWatermark(document);

		// Wait until they are all done
		while (!(documentWithWatermark.isDone())) {
			Thread.sleep(10); // 10-millisecond pause between each check
		}

		try {
			logger.info("Saving the updated document " + ticket);
			documentRepository.save(documentWithWatermark.get());
			return documentWithWatermark.get();
		} catch (ExecutionException e) {
			throw new WatermarkCreationException(ticket);
		}
	}
}
