/**
 * 
 */
package com.springer;

import org.springframework.data.jpa.repository.JpaRepository;

import com.springer.model.Document;

/**
 * CRUD Operations for Document Entity.
 * 
 * @author Miguel Moreo
 */
public interface DocumentRepository extends JpaRepository<Document, Long> {
}
