# README #

Springer: Watermark Test

This service provides an asynchronous way to obtain a document with the proper watermark.

### What is this repository for? ###

* Spring Boot REST Service
* Asynchronous
* Embedded H2 database
* JPA from CRUD Operations.
* Gradle
* JUnit
* CI: Pipelines + Gradle

* Version 1.0


### How do I get set up? ###

* Configuration
No further configuration is needed.
* Dependencies
The dependencies are managed by Gradle.
* Database configuration:
There is no need for configuration. The database is created on the fly.
The entries are not persisted.
* How to run tests:
A Junit file is provided, just execute it as a normal JUnit file.
It will mock-start the server and will launch the tests.

Manual Tests:

* Run the proyect.
* Open a Browser and go to: http://localhost:8080/watermark/
* The system will show the test data available.
* Use the ticket to ask for a document, for example: http://localhost:8080/watermark/1 
* After a moment you will obtain the document with the proper watermark
* A new call to http://localhost:8080/watermark/
Will show all the documents, and the one with ticket 1 should show the 
watermark as well.
* A new call to http://localhost:8080/watermark/1 will retrieve the 
document much faster because the watermark is already generated.



### Future Work ###

* Add capability of create/delete Documents using REST verbs: POST, DELETE...
* Use synchronize to made the service Thread-safe
* The input should be validated/controlled in a better way (only a few cases
are checked) Some Custom Exceptions were created to inform the user.

### Who do I talk to? ###

* Miguel Moreo: miguelangel.moreo@gmail.com